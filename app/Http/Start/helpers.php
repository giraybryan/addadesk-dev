<?php

/**
 * Helpers
 *
 * @package     Makent
 * @subpackage  Start
 * @category    Helper
 * @author      Trioangle Product Team
 * @version     1.0
 * @link        http://trioangle.com
 */

namespace App\Http\Start;

use View;
use Session;
use App\Models\Metas;

class Helpers
{

	// Get current controller method name
	public function current_action($route)
	{
		$current = explode('@', $route); // Example $route value: App\Http\Controllers\HomeController@login
		View::share('current_action',$current[1]); // Share current action to all view pages
	}

	// Set Flash Message function
	public function flash_message($class, $message)
	{
		Session::flash('alert-class', 'alert-'.$class);
	    Session::flash('message', $message);
	}

	// Dynamic Function for Showing Meta Details
	public static function meta($url, $field)
	{
		$metas = Metas::where('url', $url);
	
		if($metas->count())
			return $metas->first()->$field;
		else if($field == 'title')
			return 'Page Not Found';
		else
			return '';
	}

	public function compress_image($source_url, $destination_url, $quality, $max_width = 900, $max_height = 900) 
    {
        $info = getimagesize($source_url);

            if ($info['mime'] == 'image/jpeg')
                    $image = imagecreatefromjpeg($source_url);

            elseif ($info['mime'] == 'image/gif')
                    $image = imagecreatefromgif($source_url);

        elseif ($info['mime'] == 'image/png')
                    $image = imagecreatefrompng($source_url);

        $width = imagesx( $image );
        $height = imagesy( $image );


        if ($width > $height) {
            if($width < $max_width)
                $newwidth = $width;
            
            else
            
            $newwidth = $max_width; 
            
            
            $divisor = $width / $newwidth;
            $newheight = floor( $height / $divisor);
        }
        else {
            
             if($height < $max_height)
                 $newheight = $height;
             else
                 $newheight =  $max_height;
             
            $divisor = $height / $newheight;
            $newwidth = floor( $width / $divisor );
        }
        
        // Create a new'' temporary image.
        $tmpimg = imagecreatetruecolor( $newwidth, $newheight );

        imagealphablending($tmpimg, false);
        imagesavealpha($tmpimg, true);
            
        // Copy and resize old image into new image.
        imagecopyresampled( $tmpimg, $image, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);


        imagejpeg($tmpimg, $destination_url, $quality);
        return $destination_url;
    }

    public function watermark_image($img_src, $img_dest, $quality){
    	print_r($_FILES); 
        die;
        $info = getimagesize($img_src);
    	switch ($info['mime']) {
    		case 'image/jpeg':
    			$image = imagecreatefromjpeg($img_src);
    			break;
    		case 'image/gif':
    			$image = imagecreatefromgif($img_src);
    			break;
    		case 'image/png': 
    			$image = imagecreatefrompng($img_src);
    			break;
    		default:
    			return false;
    			break;
    	}
    	/*imagealphablending($image, true);
    	$img_sizes = getimagesize($img_src);

    	$overlay = imagecreatefrompng($_SERVER['DOCUMENT_ROOT'].'/images/logos/watermark.png');
    	$watermark_size = getimagesize($_SERVER['DOCUMENT_ROOT'].'/images/logos/watermark.png');
    	$offset_x = $img_sizes[0] - $watermark_size[0] - 5; 
    	$offset_y = $img_sizes[1] - $watermark_size[1] ; 
    	imagecopy($image, $overlay, $offset_x, $offset_y, 0,0, imagesx($overlay), $watermark_size[1] );*/
    	//imagepng($image, $img_dest, $quality);
       
        imagejpeg($image, $img_dest,10);
        imagejpeg($image, $img_dest,10);
    	return $img_dest;
    }

}

