<?php

/**
 * Rooms Hour Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Room Type
 * @author      Addadesk Team
 * @version     1.0
 * @link        http://addadesk.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomsHour extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rooms_hours';

    public $timestamps = false;

}