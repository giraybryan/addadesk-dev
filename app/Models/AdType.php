<?php

/**
 * Ad Type Model
 *
 * @package     addadesk
 * @subpackage  Model
 * @category    Rental Type
 * @author      bryan giray
 * @version     1.0
 * @link        http://addadesk.comm
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\AdTypePacklist;
class AdType extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ad_type';
    protected $appends = ['list'];
    public $timestamps = false;

    // Get all Active status records
    public static function active_all()
    {
    	return AdType::whereStatus('Active')->get();
    }
    //Join with rental_type table
    public function getListAttribute(){
        return $this->hasMany('App\Models\AdTypePacklist','ad_type_id','id')->where('ad_type_id', $this->attributes['id']);
    }
}
