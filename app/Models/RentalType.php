<?php

/**
 * Rantal Type Type Model
 *
 * @package     addadesk
 * @subpackage  Model
 * @category    Rental Type
 * @author      bryan giray
 * @version     1.0
 * @link        http://addadesk.comm
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RentalType extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rental_types';

    public $timestamps = false;

    // Get all Active status records
    public static function active_all()
    {
    	return RentalType::whereStatus('Active')->get();
    }
}
