<?php



/**

 * Permission Model

 *

 * @package     Makent

 * @subpackage  Model

 * @category    Permission

 * @author      Trioangle Product Team

 * @version     1.0

 * @link        http://trioangle.com

 */



namespace App\Models;



use Zizaco\Entrust\EntrustPermission;



class Permission extends EntrustPermission

{

    //

}

