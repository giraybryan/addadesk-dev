<?php

/**
 * Ad Type Model
 *
 * @package     addadesk
 * @subpackage  Model
 * @category    Rental Type
 * @author      bryan giray
 * @version     1.0
 * @link        http://addadesk.comm
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdTypePacklist extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ad_type_packlist';

    public $timestamps = false;

    // Get all Active status records
   
}
