<?php

/**
 * Washroom Type Type Model
 *
 * @package     addadesk
 * @subpackage  Model
 * @category    Washroom Type
 * @author      bryan giray
 * @version     1.0
 * @link        http://addadesk.comm
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WashRoom extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'washrooms';

    public $timestamps = false;

    // Get all Active status records
    public static function active_all()
    {
        return WashRoom::whereStatus('Active')->get();
    }
}
