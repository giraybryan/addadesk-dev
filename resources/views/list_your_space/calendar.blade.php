<div id="js-manage-listing-content-container" class="manage-listing-content-container">
      <div class="manage-listing-content-wrapper">
        <div id="js-manage-listing-content" class="manage-listing-content"><div><div id="calendar-container">
  <div class="calendar-prompt-container">
    
<div class="row-space-4">
  <div class="row">
    
      <h3 class="col-12">{{ trans('messages.lys.calendar_title') }}</h3>
    
  </div>
  <p class="text-muted">{{ trans('messages.lys.calendar_desc') }}</p>
</div>

  <div class="space-4"></div>

</div>

  <div class="calendar-settings-btn-container pull-right post-listed">
    <span class="label-contrast label-new
      hide">{{ trans('messages.lys.new') }}</span>
    <a href="#" id="js-calendar-settings-btn" class="text-normal link-icon">
      <i class="icon icon-cog text-lead"></i>
      <span class="link-icon__text">{{ trans('messages.lys.header') }}</span>
    </a>
  </div>

  <div id="calendar">
  <div class="row" id="wizard-container">
  <div class="wizard-pane row row-table">
  <div class="col-12 col-middle">
    <hr>
    <div class="row row-space-top-6">
      <h4 class="col-6">{{ trans('messages.lys.select_an_option') }}:</h4>
      <div class="col-6 text-right section-availability-dates">
        <div style="display: none;" class="js-saving-progress saving-progress">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div>

      </div>
    </div>

    <ul class="list-unstyled row text-center row-space-top-1 display-flex" ng-init="selected_calendar = '{{ lcfirst($result->calendar_type) }}'">
    
      <li class="availability-option col-4 display-flex">
        <div data-slug="always" class="option-container available-always {{ ($result->calendar_type == 'Always') ? 'selected' : '' }}" id="available-always">
          <div class="calendar-image available-always"></div>
          <p class="row-space-top-4 row-space-1">{{ trans('messages.lys.always_available') }}</p>
          <div class="h6 choice-description row-space-1">{{ trans('messages.lys.always_available_desc') }}</div>
        </div>
      </li>
    
      <li class="availability-option col-4 display-flex">
        <div data-slug="sometimes" class="option-container available-sometimes {{ ($result->calendar_type == 'Sometimes') ? 'selected' : '' }}" id="available-sometimes">
          <div class="calendar-image available-sometimes"></div>
          <p class="row-space-top-4 row-space-1">{{ trans('messages.lys.somtimes_available') }}</p>
          <div class="h6 choice-description row-space-1">{{ trans('messages.lys.somtimes_available_desc') }}</div>
        </div>
      </li>
    
      <li class="availability-option col-4 display-flex">
        <div data-slug="onetime" class="option-container available-onetime {{ ($result->calendar_type == 'Onetime') ? 'selected' : '' }}" id="available-onetime">
          <div class="calendar-image available-onetime"></div>
          <p class="row-space-top-4 row-space-1">{{ trans('messages.lys.specific_dates') }}</p>
          <div class="h6 choice-description row-space-1">{{ trans('messages.lys.specific_dates_desc') }}</div>
        </div>
      </li>
    
    </ul>
  </div>


</div>
</div>
<div class="row">
  <div class="col-sm-12">
    <hr/>
    <div class="row row-space-top-6">
      <h4 class="col-6">{{ trans('messages.lys.opening_hours') }} for {{ ($result->name == '') ? $result->sub_name : $result->name }}:</h4>
    </div>
    @if(!empty($rooms_hour))
    <div class="table-responsive">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>{{ trans('messages.lys.day') }}</th>
          <th>{{ trans('messages.lys.hours') }}</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      <?php foreach($rooms_hour as $hour): ?>
        <tr class="daywrapped">
          <td> {{ $hour->day }} </td>
          <td>
            <div class="dayhourstatus"> 
            
              <div class="caption-option caption-close {{ $hour->close == 1 ? '' : 'hide' }}">
              {{ trans('messages.lys.closed') }}
              </div>

              <div class="caption-option caption-open {{ $hour->open_24 == 1 ? '' : 'hide' }}">
              {{ trans('messages.lys.open24') }}
              </div>

              <div class="caption-option caption-hour {{ $hour->open_24 != 1 && $hour->close != 1  ? '' : 'hide' }}" >
              <span class="caption-in">{{ $hour->hour_in }}</span> - <span class="caption-out">{{ $hour->hour_out }}</span> 
              </div>
             
            </div>
            <div class="times-form hide">
              <div class="form-horizontal">
                <div class="form-group">
                  <select class="field_hourin" data-time="{{ $hour->time_in }}" data-value="{{ $hour->hour_in }}">
                    <option>{{ $hour->hour_in }}</option>
                  </select>
                  <input type="hidden" class="fieldtime_in" value="{{ $hour->time_in}}">
                </div>
                <div style='margin: 5px; text-align: center'>To</div>
                <div class="form-group">
                  <select class="field_hourout" data-time="{{ $hour->time_out }}" data-value="{{ $hour->hour_out }}">
                    <option data-value>{{ $hour->hour_out }}</option>
                  </select>
                  <input type="hidden" class="fieldtime_out" value="{{ $hour->time_out}}">
                </div>
              </div>
            </div>
            <input type="hidden" class="fieldclose" value="{{ $hour->close }}">
            <input type="hidden" class="fieldopen_24" value="{{ $hour->open_24 }}">
            <input type="hidden" class="hourid" value="{{ $hour->id}}">
          </td>
          <td>
            <span class="closeday  {{ $hour->close == 1 ? 'active' : '' }}" data-target=""> {{ trans('messages.lys.closed') }} </span>
          </td>
          <td>
            <span class="always-open {{ $hour->open_24 == 1 ? 'active' : '' }}"> {{ trans('messages.lys.open24') }} </span>
          </td>
        </tr>
      <?php endforeach;?>
      </tbody>
    </table>
    </div>
    @endif
  </div>
</div>

<div id="calendar-settings-container" class="row-space-6 hide">
  <hr>
  
<div class="js-section">
  <div style="display: none;" class="js-saving-progress saving-progress">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div>

  <h4>{{ trans('messages.lys.calendar_settings') }}</h4>

  <div class="row">
    <div class="col-5">
      <div id="min-max-nights-container" class="row"></div>
      <div id="advance-notice-container" class="row row-space-top-3"></div>
    </div>
  </div>
  
</div>

</div>

<div id="calendar-wizard-navigation">
  <div class="not-post-listed row row-space-top-6 progress-buttons">
  <div class="col-12">
    <div class="separator"></div>
  </div>
  @if($result->status == NULL)
  <div class="col-2 row-space-top-1 next_step">
    
      <a data-prevent-default="" href="{{ url('manage-listing/'.$room_id.'/pricing') }}" class="back-section-button">{{ trans('messages.lys.back') }}</a>
    
  </div>
  @endif

 
  <div class="col-10 text-right">
    
      <a data-prevent-default="" href="" class="{{ $result->steps_count == 0 ? 'hide' : 'hide' }} btn btn-large btn-primary remaining-steps-section-button" id="finish_step">
        {{ trans('messages.lys.finish_remaining_steps') }}
      </a>
      <button class="btn btn-large btn-primary disable-btn" disabled="">{{ trans('messages.lys.finish_remaining_steps') }}</button>

       <button class="btn btn-large btn-primary btn-listing {{ $result->steps_count == 0 ? '' : 'hide' }} ">  {{ trans('messages.lys.list_space') }} </button>
  </div>

   @if($result->status != NULL)
  <div class="col-10 text-right next_step">
  
      <a class="btn btn-large btn-primary next-section-button" href="{{ url('manage-listing/'.$room_id.'/pricing') }}" data-prevent-default="">
        {{ trans('messages.lys.next') }}
      </a>
  </div>
  @endif
  
</div>

</div>
</div>
</div>

<div class="pricing-tips-sidebar-container"></div>
</div></div>
      </div>
      <div class="manage-listing-content-background"></div>
    </div>


