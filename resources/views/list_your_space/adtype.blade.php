
    <div class="manage-listing-content-container" id="js-manage-listing-content-container" style="top: -22px;">
      <div class="manage-listing-content-wrapper">
        <div class="manage-listing-content" style="width: 900px; max-width: 100%;" id="js-manage-listing-content"><div>
  
<div class="row-space-4">
  <div class="row">
      <h3 class="col-12">{{ trans('messages.lys.ad_type') }} for {{ ($result->name == '') ? $result->sub_name : $result->name }}</h3>
  </div>
  <p class="text-muted">{{ trans('messages.lys.ad_desc',['site_name'=>$site_name]) }}</p>
</div>

    <form name="overview">
    <div class="js-section" ng-init="name='{{ $result->name }}'; summary='{{ $result->summary }}'">
      <div class="js-saving-progress saving-progress" style="display: none;">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div>
<style type="text/css">
  .package-title { font-size: 19px; }
  .check-icon {  position: absolute; right: 20px; top: 10px; color: #87e06b; font-size: 37px; display: none; text-shadow: 1px 1px 1px #555252; }
  .package-option:hover .check-icon, .package-option.selected .check-icon { display: block;  }
  .display-flex { position: relative; }
  .package-list .option-container { padding-top: 0px; }
  .package-title { margin-top: 0px; padding: 15px 5px; }
  .package-1 { background-color: #e8e8e8; }
  .package-2 {
    background: #267b9e;
      color: #fff;
  }
  .package-3 { background-color: #fea01e; color: #fff; }
  .package-4 { background-color: #9fa33d; color: #fff; }
  .package-option {
    -webkit-transition: margin-top 1s; /* For Safari 3.1 to 6.0 */
    transition: margin-top 1s;
  }
  .package-option:hover {
    margin-top: -10px;
  }
  .ad_price {
    font-weight: 500; font-size: 33px;
  }
</style>
  <div id="calendar">
  <div class="row" id="wizard-container">
  <div class="wizard-pane row row-table">
    <div class="col-12 col-middle">
      <hr>
        
      @if( $result->status != 'Listed' )
      <div class="not-post-listed row  progress-buttons">
  <div class="col-12">
    <div class="separator"></div>
  </div>
  <div class="col-2 row-space-top-1 next_step">
    
    
  </div>
  <div class="col-10 text-right next_step col-mobile-block">
    
    
      <a class="mobile-block-width btn btn-large btn-primary next-section-button level2 {{ $result->ad_type_id < 1 ? 'hide' : '' }} " href="{{ url('manage-listing/'.$room_id.'/basics') }}" data-prevent-default="">
        {{ trans('messages.lys.next') }}
      </a>
    
  </div>
</div>
      <div class="row row-space-top-6">
          <h4 class="col-6">{{ trans('messages.lys.ad_type_desc') }}:</h4>
          <div class="col-6 text-right section-availability-dates">
            <div style="display: none;" class="js-saving-progress saving-progress">
      <h5>{{ trans('messages.lys.saving') }}...</h5>
    </div>

          </div>
        </div>

      <ul class="package-list list-unstyled row text-center row-space-top-1 display-flex" ng-init="selected_adtype = '{{ $result->ad_type_id }}'">
        @foreach( $adtypes as $adtype )

        <li class="package-option {{ $result->ad_type_id == $adtype->id ? 'selected' : '' }} availability-option col-3 display-flex">
          
          <div  style='padding-top: 0px;' data-slug="{{$adtype->id}}" class="option-container  available-always {{ ($result->ad_type_id   == $adtype->id) ? 'selected' : '' }}" id="adtype-{{$adtype->id}}">
            <div class="row">
              <p class="row-space-top-4 row-space-1 package-title package-{{ $adtype->id}}">{{ $adtype->name }}</p>
            </div>
          <i class="check-icon fa fa-check-circle"></i>
          
            <div class="ad_price"> {{ $currency_symbol }} {{ $adtype->price }}</div>
            <div class="h6 choice-description row-space-1">{{ $adtype->desc }}</div>
            <hr/>
            <div>
              <ul style='text-align: left;'>
              @foreach( $adtype->packlist as $list )
                <li> {{$list->desc}} </li>
              @endforeach
              </ul>
              
            </div>
          </div>
        </li>
        @endforeach
      @else
      <div class="row row-space-top-6">
          <h4 class="col-6">You Selected:</h4>
          <div class="col-6 text-right section-availability-dates">
            <div style="display: none;" class="js-saving-progress saving-progress">
      <h5>{{ trans('messages.lys.saving') }}...</h5>
    </div>

          </div>
        </div>
      <ul class="package-list list-unstyled row text-center row-space-top-1 display-flex" ng-init="selected_adtype = '{{ $result->ad_type_id }}'">
        <li class="package-option {{ $result->ad_type_id == $adtype->id ? 'selected' : '' }} availability-option col-3 display-flex">
          
          <div  style='padding-top: 0px;' data-slug="{{$adtype->id}}" class="option-container  available-always {{ ($result->ad_type_id   == $adtype->id) ? 'selected' : '' }}" id="adtype-{{$adtype->id}}">
            <div class="row">
              <p class="row-space-top-4 row-space-1 package-title package-{{ $adtype->id}}">{{ $adtype->name }}</p>
            </div>
          <i class="check-icon fa fa-check-circle"></i>
          
            <div class="ad_price"> {{ $currency_symbol }} {{ $adtype->price }}</div>
            <div class="h6 choice-description row-space-1">{{ $adtype->desc }}</div>
            <hr/>
            <div>
              <ul style='text-align: left;'>
              @foreach( $adtype->packlist as $list )
                <li> {{$list->desc}} </li>
              @endforeach
              </ul>
              
            </div>
          </div>
        </li>
      </ul>
      @endif
        
        </li>
      
      </ul>
      </div>
      </div>
    </div>
    </div>
    </div>
    </form>
    
    <div class="not-post-listed row row-space-top-6 progress-buttons">
  <div class="col-12">
    <div class="separator"></div>
  </div>
  <div class="col-2 row-space-top-1 next_step">
    
    
  </div>
  <div class="col-10 text-right next_step col-mobile-block">
    
    
      <a class="mobile-block-width btn btn-large btn-primary next-section-button level2 {{ $result->ad_type_id < 1 ? 'hide' : '' }} " href="{{ url('manage-listing/'.$room_id.'/basics') }}" data-prevent-default="">
        {{ trans('messages.lys.next') }}
      </a>
    
  </div>
</div>

  



</div></div>
<!--
        <div class="manage-listing-help" id="js-manage-listing-help"><div class="manage-listing-help-panel-wrapper">
  <div class="panel manage-listing-help-panel" style="top: 166px;">
    <div class="help-header-icon-container text-center va-container-h">
      {!! Html::image('images/lightbulb2x.png', '', ['class' => 'col-center', 'width' => '50', 'height' => '50']) !!}
    </div>
    <div class="panel-body">
      <h4 class="text-center">{{ trans('messages.lys.listing_name') }}</h4>
      
  <p>{{ trans('messages.lys.listing_name_desc') }}</p>
  <p>{{ trans('messages.lys.example_name') }}</p>

    </div>
  </div>
</div>

</div> -->
      </div>
      <div class="manage-listing-content-background"></div>
    </div>