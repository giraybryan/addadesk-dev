
 <div id="gmap-preload" class="hide"></div>
   
   <div class="ipad-interstitial-wrapper"><span data-reactid=".1"></span></div>
 
    <div id="fb-root"></div>
 
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key={{ $map_key }}&sensor=false&libraries=places"></script>

    {!! Html::script('js/jquery-1.11.3.js') !!}
    {!! Html::script('js/jquery-ui.js') !!}
    {!! Html::script('js/i18n/datepicker-'.Session::get('language').'.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}

    {!! Html::script('js/angular.js') !!}
    {!! Html::script('js/angular-sanitize.js') !!}
    
    <script type="text/javascript">
    $(document).ready(function() {
      $('.login_popup_head').click(function(e){
        e.preventDefault();
           $("body").addClass("pos-fix");
          $(".login_popup").show();
          $(".signup_popup").hide();
          $(".signup_popup2").hide(); 
      });
      $('.login-close').click(function(event){
            $("body").removeClass("pos-fix");
        $(".login_popup").hide(); 
      });
      $('.top-home').click(function(event){
          event.stopPropagation();
      });
    });
    </script>

    <script type="text/javascript">
      $(document).ready(function() {
            $('.forgot-password-popup').click(function(e){
              e.preventDefault();
                 $("body").addClass("pos-fix");
                $(".login_popup").hide();
                $(".forgot-passward").show();
            });
             $('.login-close').click(function(event){
                    $("body").removeClass("pos-fix");
                $(".forgot-passward").hide(); 
            });
            $('.top-home').click(function(event){
                event.stopPropagation();
            });
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
            $('.signup_popup_head').click(function(e){
               e.preventDefault();
                $("body").addClass("pos-fix");
                $(".signup_popup").show();
                 $(".login_popup").hide();  
            });
             $('.login-close').click(function(){
                 $("body").removeClass("pos-fix");
                $(".signup_popup").hide(); 
            });
             $('.top-home').click(function(event){
    event.stopPropagation();
});
         });
    </script>
     <script type="text/javascript">
      $(document).ready(function() {
            $('.signup_popup_head2').click(function(e){
               e.preventDefault();
                   $("body").addClass("pos-fix");
                $(".signup_popup2").show(); 
                 $(".signup_popup").hide(); 
            });
             $('.login-close').click(function(){
                 $("body").removeClass("pos-fix");
                $(".signup_popup2").hide(); 
            });
             $('.top-home').click(function(event){
    event.stopPropagation();
});
         });
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
            $('ul.menu-group li a').click(function()
            {
                $('.nav--sm').css('visibility','hidden');
            });
          });
    </script>
    <script> 
    var app = angular.module('App', ['ngSanitize']);
    var APP_URL = {!! json_encode(url('/')) !!};
    var USER_ID = {!! @Auth::user()->user()->id !!}
    $.datepicker.setDefaults($.datepicker.regional[ "{{ (Session::get('language')) ? Session::get('language') : $default_language[0]->value }}" ])
    </script>

    {!! $head_code !!}

    {!! Html::script('js/common.js') !!}
    {!! Html::script('js/datetime_function.js') !!}
    @if (!isset($exception))

      @if (Route::current()->uri() == 'rooms/new')
        {!! Html::script('js/rooms_newun.js') !!}
      @endif
      @if (Route::current()->uri() == 'offices/new')
        {!! Html::script('js/rooms_newun.js') !!}
      @endif

      @if (Route::current()->uri() == 'manage-listing/{id}/{page}')
        {!! Html::script('js/manage_listingun.js') !!}
      @endif

      @if (Route::current()->uri() == 's')
        {!! Html::script('js/searchun.js') !!}
		{!! Html::script('js/infobubble.js') !!}
      @endif

      @if (Route::current()->uri() == 'rooms/{id}')
        {!! Html::script('js/roomsun.js') !!}
        {!! Html::script('js/jquery.bxslider.min.js') !!}
      @endif
      @if (Route::current()->uri() == 'offices/{id}')
        {!! Html::script('js/roomsun.js') !!}
        {!! Html::script('js/jquery.bxslider.min.js') !!}
      @endif

      @if (Route::current()->uri() == 'reservation/change')
        {!! Html::script('js/reservation.js') !!}
      @endif

      @if (Route::current()->uri() == 'wishlists/popular' || Route::current()->uri() == 'wishlists/my' || Route::current()->uri() == 'wishlists/{id}' || Route::current()->uri() == 'users/{id}/wishlists')
        {!! Html::script('js/wishlists.js') !!}
      @endif

      @if (Route::current()->uri() == 'inbox' || Route::current()->uri() == 'z/q/{id}' || Route::current()->uri() == 'messaging/qt_with/{id}')
        {!! Html::script('js/inbox.js') !!}
      @endif

      @if (Route::current()->uri() == 'reservation/{id}')
        {!! Html::script('js/reservation.js') !!}
      @endif
      @if (Route::current()->uri() == 'tour_request/{id}')
        {!! Html::script('js/tour_request.js') !!}
      @endif

    @endif

    @stack('scripts')
  @if(!empty(Session::get('error_code')) && Session::get('error_code') == 5)
<script>
$(function() {
    $('.login_popup').show();
    $('.signup_popup').hide();
    $('.signup_popup2').hide();
    $('.forgot-passward').hide();
});
</script>
@endif
@if(!empty(Session::get('error_code')) && Session::get('error_code') == 1)
<script>
$(function() {  
    $('.login_popup').hide();
    $('.signup_popup2').show();
    $('.signup_popup').hide();
    $('.forgot-passward').hide();
  });
</script>
@endif

 @if(!empty(Session::get('error_code')) && Session::get('error_code') == 4)
<script>
$(function() {
     $('.login_popup').hide();
    $('.signup_popup').hide();
    $('.signup_popup2').hide();
    $('.forgot-passward').show();
});
</script>
@endif
<!-- ver. 87c23752f8dfbd60bf83837d2c8b2dcd0ec660a9 -->
<div class="tooltip tooltip-bottom-middle" role="tooltip" aria-hidden="true">  
	<p class="panel-body">To sign up, you must be 18 or older. Other people won’t see your birthday.</p>
	</div></body></html>