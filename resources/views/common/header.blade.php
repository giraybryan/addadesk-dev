   
<div id="header" class="makent-header new {{ (!isset($exception)) ? (Route::current()->uri() == '/' ? 'shift-with-hiw' : '') : '' }}">    
    <header class="header--sm show-sm" aria-hidden="true" role="banner">
  <a href="javascript:void(0);" aria-label="Homepage" data-prevent-default="" class="link-reset burger--sm">
    <i class="icon icon-reorder icon-rausch"></i>
    <span class="screen-reader-only">
      {{ $site_name }}
    </span>
  </a>
  <div class="title--sm text-center">
      @if(!isset($exception)) 
        @if(Route::current()->uri() == '/' || Request::segment(1) == 'help')
          <a href="{{ url() }}" class="header-belo" style="{{ $logo_style }}">
            <span class="screen-reader-only">
              {{ $site_name }}
            </span>
          </a>
        @elseif(Request::segment(1) != 'help')
          <button class="btn btn-block search-btn--sm search-modal-trigger">
            <i class="icon icon-search icon-gray"></i>
            <span class="search-placeholder--sm">
              {{ trans('messages.header.where_are_you_going') }}
            </span>
          </button>
        @endif
      @endif
  </div>
  <div class="action--sm"></div>
  <nav role="navigation" class="nav--sm"><div class="nav-mask--sm"></div>
<div class="nav-content--sm panel text-white">
  <div class="nav-header {{ (Auth::user()->user()) ? '' : 'items-logged-in' }}">
    <div class="nav-profile clearfix">
      <div class="user-item pull-left">
        <a href="{{ url() }}/users/show/{{ (Auth::user()->user()) ? Auth::user()->user()->id : '0' }}" class="link-reset user-profile-link">
          <div class="media-photo media-round user-profile-image" style="background:rgba(0, 0, 0, 0) url({{ (Auth::user()->user()) ? Auth::user()->user()->profile_picture->header_src : '' }}) no-repeat scroll 0 0 / cover">
          </div>
          {{ (Auth::user()->user()) ? Auth::user()->user()->first_name : 'User' }}
        </a>
      </div>
    </div>
    <hr>
  </div>
  <div class="nav-menu-wrapper">
    <div class="va-container va-container-v va-container-h">
      <div class="va-middle nav-menu panel-body">
        <ul class="menu-group list-unstyled row-space-3 margin-bottom-0">
          <li>
            <a rel="nofollow" class="link-reset menu-item" href="{{ url() }}">
              {{ trans('messages.header.home') }}
            </a>
          </li>
          <li class="{{ (Auth::user()->user()) ? 'items-logged-out' : '' }}">
            <a rel="nofollow" class="link-reset menu-item signup_popup_head" href="{{ url() }}/signup_login" data-signup-modal="">
              {{ trans('messages.header.signup') }}
            </a>
          </li>
          <li class="{{ (Auth::user()->user()) ? 'items-logged-out' : '' }}">
            <a rel="nofollow" class="link-reset menu-item login_popup_head" href="{{ url() }}/login" data-login-modal="">
              {{ trans('messages.header.login') }}
            </a>
          </li>
          <li class="{{ (Auth::user()->user()) ? '' : '' }}">
            <a class="link-reset menu-item" rel="nofollow" href="{{ url() }}/offices/new">
              {{ trans('messages.header.list_your_space') }}
            </a>
          </li>
          <li class="{{ (Auth::user()->user()) ? '' : 'items-logged-in' }}">
            <a class="link-reset menu-item" rel="nofollow" href="{{ url() }}/inbox">
              {{ trans('messages.header.inbox') }}
              <i class="alert-count unread-count--sm fade text-center">
                0
              </i>
            </a>
          </li>
          <li class="{{ (Auth::user()->user()) ? '' : 'items-logged-in' }}">
            <a class="link-reset menu-item" rel="nofollow" href="{{ url() }}/offices">
              {{ trans_choice('messages.header.your_listing', 2) }}
            </a>
          </li>
          <li class="{{ (Auth::user()->user()) ? '' : 'items-logged-in' }}">
            <a class="link-reset menu-item" rel="nofollow" href="{{ url() }}/trips/current">
              {{ trans('messages.header.your_trips') }}
            </a>
          </li>
          <li>
            <a class="search-modal-trigger link-reset menu-item" data-prevent-default="" rel="nofollow" href="{{ url() }}/s">
              {{ trans('messages.home.where_do_you_go1') }}
            </a>
          </li>
        </ul>
        
        <ul class="menu-group list-unstyled row-space-3">
          <li>
            <a class="link-reset menu-item" rel="nofollow" href="{{ url() }}/help">
              {{ trans('messages.header.help') }}
            </a>
          </li>
          <!--
          <li>
            <a class="link-reset menu-item" rel="nofollow" href="{{ url() }}/invite">
              {{ trans('messages.header.invite_friends') }}
            </a>
          </li>
          -->
          <li class="{{ (Auth::user()->user()) ? '' : 'items-logged-in' }}">
            <a class="link-reset menu-item logout" rel="nofollow" href="{{ url() }}/logout">
              {{ trans('messages.header.logout') }}
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
</nav>
  <div class="search-modal-container">
  <div class="modal hide" role="dialog" id="search-modal--sm" aria-hidden="false" tabindex="-1">
  <div class="modal-table">
    <div class="modal-cell">
      <div class="modal-content">
        <div class="panel-header modal-header">
          <a href="#" class="modal-close" data-behavior="modal-close">
            <span class="screen-reader-only">{{ trans('messages.home.close') }}</span>
            <span class="aria-hidden"></span>
          </a>
          {{ trans('messages.home.search') }}
        </div>
        <div class="panel-body">
          <div class="modal-search-wrapper--sm">
              <input type="hidden" name="source" value="mob">
              <div class="row">
                  <div class="searchbar__location-error hide" style="left:22px; top:2%;">{{ trans('messages.home.search_validation') }}</div>
                <div class="col-sm-12">
                  <label for="header-location--sm">
                    <span class="screen-reader-only">{{ trans('messages.header.where_are_you_going') }}</span>
                    <input type="text" placeholder="{{ trans('messages.header.where_are_you_going') }}" autocomplete="off" name="location" id="header-location--sm" class="input-large" value="{{ @$location }}">
                  </label>
                </div>
              </div>
              
              <div class="row space-2 space-top-1">
                <div class="col-sm-12">
                  <label for="header-search-guests" class="screen-reader-only">
                    {{ trans('messages.home.no_of_guests') }}
                  </label>
                  <div class="select select-block select-large">
                    <select id="modal_guests" name="guests--sm">
                    @for($i=1;$i<=16;$i++)
                      <option value="{{ $i }}" {{ ($i == @$guest) ? 'selected' : '' }}>{{ $i }} desk{{ ($i>1) ? 's' : '' }}</option>
                    @endfor
                    </select>
                  </div>
                </div>
              </div>
              <div class="panel room-type-filter--sm row-space-top-1">
                <div class="panel-body">
                  <div class="row text-center">
                   @foreach($header_room_type as $row)
                    <input type="checkbox" id="room-type-{{ $row->id }}--sm" name="room_types[]" value="{{ $row->id }}" {{ (@in_array($row->id,@$room_type_selected)) ? 'checked' : '' }}>
                    <label class="col-sm-4 modal-filter needsclick" for="room-type-{{ $row->id }}--sm">
                    @if($row->id == 1)
                    <i class="icon icon-entire-place icon-size-2 needsclick"></i>
                    @endif
                    @if($row->id == 2)
                    <i class="icon icon-private-room icon-size-2 needsclick"></i>
                    @endif
                    @if($row->id == 3)
                    <i class="icon icon-shared-room icon-size-2 needsclick"></i>
                    @endif
                    <br>{{ $row->name }}
                    </label>
                    @endforeach
                  </div>
                </div>
              </div>
              <div class="row row-space-top-2">
                <div class="col-sm-12">
                  <button type="submit" id="search-form--sm-btn" class="btn btn-primary btn-large btn-block">
                    <i class="icon icon-search"></i>
                    {{ trans('messages.header.find_place') }}
                  </button>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</header>


    <header class="regular-header clearfix hide-sm" id="old-header" role="banner">
  <a aria-label="Homepage" href="{{ url() }}" class="header-belo pull-left {{ (!isset($exception)) ? (Route::current()->uri() == '/' ? 'home-logo' : '') : '' }}" style="{{ (!isset($exception)) ? (Route::current()->uri() == '/' ? $home_logo_style : $logo_style) : $logo_style }}">
    <span class="screen-reader-only">
      {{ $site_name }}
    </span>
  </a>
<!-- hide-sm -->
@if(Request::segment(1) != 'help')
  <ul class="nav pull-left  list-unstyled search-form-container" id="search-form-header">
  <li id="header-search" class="search-bar-wrapper pull-left medium-right-margin">
  
</li>

  <li class="dropdown-trigger pull-left large-right-margin hide-sm hide" data-behavior="recently-viewed__container">
    <a class="no-crawl link-reset" href="{{ url() }}/login?_ga=1.237472128.1006317855.1436675116#" data-href="/history#docked-filters" data-behavior="recently-viewed__trigger">
      <span class="show-lg recently-viewed__label">
        {{ trans('messages.header.recently_viewed') }}
      </span>
      <span class="hide-lg recently-viewed__label">
        <i class="icon icon-recently-viewed icon-gray h4"></i>
      </span>
      <i class="icon icon-caret-down icon-light-gray h5"></i>
    </a>
    <div class="tooltip tooltip-top-left dropdown-menu recently-viewed__dropdown" data-behavior="recently-viewed__dropdown">
    </div>
  </li>
  <!-- show-md -->
    <li class="browse-container dropdown-trigger pull-left">
      <a class="link-reset header-browse-trigger" href="#" data-prevent-default="" id="header-browse-trigger">
  {{ trans('messages.header.browse') }}
   <i class="icon icon-caret-down icon-light-gray h5"></i>
</a>
<ul class="tooltip tooltip-top-left dropdown-menu list-unstyled">
  <li>
    <a href="{{ url('wishlists/popular') }}" class="link-reset menu-item header-browse-popular">
      {{ trans('messages.header.popular') }}
    </a>
  </li>
  <li>
    <a href="{{ url('locations') }}" class="hide link-reset menu-item header-browse-neighborhoods">
      {{ trans('messages.header.neighborhoods') }}
    </a>
  </li>
</ul>

    </li>
</ul>
  @endif

  <ul class="nav pull-right help-menu list-unstyled">
  @if(Request::segment(1) != 'help')
  <li id="header-help-menu" class="help-menu-container pull-left large-right-margin dropdown-trigger">
    <a class="help-toggle link-reset" href="{{ url('help') }}">
      {{ trans('messages.header.help') }}
    </a>
  </li>
  @endif
  <!-- <li id="header-help-menu" class="help-menu-container pull-left large-right-margin hide-md dropdown-trigger">
    <a class="help-toggle link-reset" href="{{ url('help') }}">
      {{ trans('messages.header.help') }}
      <i class="icon icon-caret-down icon-light-gray h5"></i>
    </a>
    <div class="tooltip tooltip-top-right dropdown-menu help-dropdown">
      <ul class="list-layout">
  <li class="search-bar--help hide">
    <form action="{{ url('help/search') }}" class="search-form--help menu-item">
      <div class="input-addon">
        <input type="text" placeholder="Search the help center" autocomplete="off" name="q" class="input-stem">
        <input type="hidden" name="search_from" value="hdr">
        <i class="btn btn-primary icon icon-search input-suffix form-submit--help"></i>
      </div>
    </form>
  </li>
  <li class="visit-helpcenter-cta hide">
    <a href="{{ url('help') }}" class="help-center-link menu-item">
      {{ trans('messages.header.visit_help_center') }} »
    </a>
  </li>
  <li class="loading"></li>
  <li class="all_faqs hide">
    <a href="{{ url('help') }}" class="link-reset menu-item">
      {{ trans('messages.header.see_all_faqs') }}
    </a>
  </li>
</ul>
    </div>
  </li> -->
    <li class="list-your-space pull-left">
        <a id="list-your-space" class="btn btn-special list-your-space-btn" href="{{ url('offices/new') }}">
          {{ trans('messages.header.list_your_space') }}
        </a>
    </li>
</ul>


@if(!Auth::user()->check())
  <ul class="nav pull-right logged-out list-unstyled medium-right-margin">
  <li id="sign_up" class="pull-left medium-right-margin">
    <a data-signup-modal="" data-header-view="true" href="{{ url('signup_login') }}" data-redirect="" class="link-reset signup_popup_head">
      {{ trans('messages.header.signup') }}
    </a>
  </li>
  <li id="login" class="pull-left">
    <a data-login-modal="" href="{{ url('login') }}" data-redirect="" class="link-reset login_popup_head">
      {{ trans('messages.header.login') }}
    </a>
  </li>
</ul>
@endif

@if(Auth::user()->check())
  <ul class="nav pull-right list-unstyled medium-right-margin">
  <li id="inbox-item" class="inbox-item pull-left dropdown-trigger js-inbox-comp">
    <a href="{{ url('inbox') }}" rel="nofollow" class="no-crawl link-reset inbox-icon">
      <i class="alert-count text-center {{ (Auth::user()->user()->inbox_count()) ? '' : 'fade' }}">{{ Auth::user()->user()->inbox_count() }}</i>
      <i class="icon icon-envelope icon-gray">
        <span class="text-hide hide">
          {{ trans('messages.header.inbox') }}
        </span>
      </i>
    </a>
    <div class="tooltip tooltip-top-right dropdown-menu list-unstyled header-dropdown
                notifications-dropdown hide">
    </div>
  </li>
</ul>


  <ul class="nav pull-right list-unstyled" role="navigation">
  <li class="user-item pull-left medium-right-margin dropdown-trigger">
    <a class="link-reset header-avatar-trigger" id="header-avatar-trigger" href="{{ url('login') }}">
      <div class="media-photo media-round user-profile-image" style="background: rgba(0, 0, 0, 0) url({{ Auth::user()->user()->profile_picture->header_src }}) no-repeat scroll 0 0 / cover" ></div>
      <span class="value_name">
        {{ Auth::user()->user()->first_name }}
      </span>
      <i class="icon icon-caret-down icon-light-gray h5"></i>
    </a>
    <ul class="tooltip tooltip-top-right dropdown-menu list-unstyled header-dropdown">
      <li>
        <a href="{{ url('dashboard') }}" rel="nofollow" class="no-crawl link-reset menu-item item-dashboard">
          {{ trans('messages.header.dashboard') }}
        </a>
      </li>
      <li class="listings">
        <a href="{{ url('offices') }}" rel="nofollow" class="no-crawl link-reset menu-item item-listing">
          <span class="plural">
            {{ trans_choice('messages.header.your_listing',2) }}
          </span>
        </a>
      </li>
      <li class="reservations" style="display: none;">
        <a href="{{ url('my_reservations') }}" rel="nofollow" class="no-crawl link-reset menu-item item-reservation">
           {{ trans('messages.header.your_reservations') }}
        </a>
      </li>
      <li>
        <a href="{{ url('trips/current') }}" rel="nofollow" class="no-crawl link-reset menu-item item-trips">
          {{ trans('messages.header.your_trips') }}
        </a>
      </li>
      @if(Auth::user()->user()->wishlists)
      <li>
        <a href="{{ url('wishlists/my') }}" id="wishlists" class="no-crawl link-reset menu-item item-wishlists">
          {{ trans_choice('messages.header.wishlist',2) }}
        </a>
      </li>
      @endif
      <li class="groups hide">
        <a href="{{ url('groups') }}" rel="nofollow" class="no-crawl link-reset menu-item item-groups">
          {{ trans('messages.header.groups') }}
        </a>
      </li>
      <li>
        <a href="{{ url('invite') }}" class="no-crawl link-reset menu-item item-invite-friends">
          {{ trans('messages.referrals.travel_credit') }}
          <span class="label label-pink label-new">
          </span>
        </a>
      </li>
      <li>
        <a href="{{ url('users/edit') }}" rel="nofollow" class="no-crawl link-reset menu-item item-user-edit">
         {{ trans('messages.header.edit_profile') }}
        </a>
      </li>
      <li>
        <a href="{{ url('account') }}" rel="nofollow" class="no-crawl link-reset menu-item item-account">
          {{ trans('messages.header.account') }}
        </a>
      </li>
      <li class="business-travel hide">
        <a href="{{ url('business') }}" rel="nofollow" class="no-crawl link-reset menu-item item-business-travel">
          {{ trans('messages.header.business_travel') }}
        </a>
      </li>
      <li>
        <a href="{{ url('logout') }}" rel="nofollow" class="no-crawl link-reset menu-item header-logout">
          {{ trans('messages.header.logout') }}
        </a>
      </li>
    </ul>
  </li>
</ul>
@endif

</header>
</div>

<div class="flash-container">
@if(Session::has('message') && (!Auth::user()->check() || Route::current()->uri() == 'offices/{id}' || Route::current()->uri() == 'payments/book/{id?}'))
  <div class="alert {{ Session::get('alert-class') }}" role="alert">
    <a href="#" class="alert-close" data-dismiss="alert"></a>
  {{ Session::get('message') }}
  </div>
@endif
</div>
<div class="login-close">
<div class="login_popup">
<div class="page-container-responsive page-container-auth row-space-top-4 row-space-8 margintop">
  <div class="row">
    <div class="col-md-6 col-lg-5 col-center">
      <div class="panel top-home bor-none">
         <!-- <div class="login-close">
        <img src="images/close.png">
      </div>-->
          <div class="panel-body pad-25 bor-none">
<a href="{{ $fb_url }}" class="fb-button fb-blue btn icon-btn btn-block btn-large row-space-1 btn-facebook font-normal pad-top" data-populate_uri="" data-redirect_uri="{{URL::to('/')}}/authenticate">
  <span class="icon-container">
    <i class="icon icon-facebook"></i>
  </span>
  <span class="text-container">
    {{ trans('messages.login.login_with') }} Facebook
  </span>
</a>

  
<a href="{{URL::to('googleLogin')}}" class="btn icon-btn btn-block btn-large row-space-1 btn-google font-normal pad-top">
  <span class="icon-container">
    <i class="icon icon-google-plus"></i>
  </span>
  <span class="text-container">
    {{ trans('messages.login.login_with') }} Google
  </span>
</a>


  <div class="signup-or-separator">
    <span class="h6 signup-or-separator--text">{{ trans('messages.login.or') }}</span>
    <hr>
  </div>

  {!! Form::open(['action' => 'UserController@authenticate', 'class' => 'signup-form login-form', 'data-action' => 'Signin', 'accept-charset' => 'UTF-8' , 'novalidate' => 'true']) !!}

  {!! Form::hidden('from', 'email_login', ['id' => 'from']) !!}
  
  <div class="control-group row-space-1">

    @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif

  {!! Form::email('email', '', ['class' => $errors->has('email') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore', 'placeholder' => trans('messages.login.email_address'), 'id' => 'signin_email']) !!}

  </div>
  <div class="control-group row-space-2 position-relative">

    @if ($errors->has('password'))
     <p class="help-block">{{ $errors->first('password') }}</p>
     @endif

  {!! Form::password('password', ['class' => $errors->has('password') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore', 'placeholder' => trans('messages.login.password'), 'id' => 'signin_password', 'data-hook' => 'signin_password']) !!}
   <span class="showpw fa fa-eye"></span>

  </div>

  <div class="clearfix row-space-2">
    <label for="remember_me2" class="checkbox remember-me">
      {!! Form::checkbox('remember_me', '1', false, ['id' => 'remember_me2', 'class' => 'remember_me']) !!}
      {{ trans('messages.login.remember_me') }}
    </label>
    <a href="{{URL::to('/')}}/forgot_password" class="forgot-password forgot-password-popup  pull-right">{{ trans('messages.login.forgot_pwd') }}</a>
  </div>

  {!! Form::submit(trans('messages.header.login'), ['class' => 'btn btn-primary btn-block btn-large pad-top', 'id' => 'user-login-btn']) !!}

</form>
          </div>
          <div class="panel-body">
            {{ trans('messages.login.dont_have_account') }}
            <a href="{{ url('signup_login') }}" class="link-to-signup-in-login login-btn signup_popup_head">
              {{ trans('messages.header.signup') }}
            </a>
          </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="login-close">
<div class="signup_popup">
<div class="page-container-responsive page-container-auth row-space-top-4 row-space-8 margintop">
  <div class="row">
    <div class="col-md-6 col-lg-5 col-center">
      <div class="panel top-home bor-none">
        <!--<div class="login-close">
        <img src="images/close.png">
      </div>-->
<div class="alert alert-with-icon alert-error alert-header panel-header hidden-element notice" id="notice">
  <i class="icon alert-icon icon-alert-alt"></i>
  
</div>
<div class="panel-padding panel-body pad-25 ">

  <div class="social-buttons">

      
<a href="{{ $fb_url }}" class="fb-button fb-blue btn icon-btn btn-block row-space-1 btn-large btn-facebook pad-23" data-populate_uri="" data-redirect_uri="{{URL::to('/')}}/authenticate">
  <span class="icon-container">
    <i class="icon icon-facebook"></i>
  </span>
  <span class="text-container">
    {{ trans('messages.login.signup_with') }} Facebook
  </span>
</a>

      
<a href="{{URL::to('googleLogin')}}" class="btn icon-btn btn-block row-space-1 btn-large btn-google pad-23">
  <span class="icon-container">
    <i class="icon icon-google-plus"></i>
  </span>
  <span class="text-container">
    {{ trans('messages.login.signup_with') }} Google
  </span>
</a>
  </div>

  <div class="text-center social-links hide">
    {{ trans('messages.login.signup_with') }} <a href="{{ $fb_url }}" class="facebook-link-in-signup">Facebook</a> {{ trans('messages.login.or') }} <a href="{{URL::to('googleLogin')}}">Google</a>
  </div>

    <div class="signup-or-separator">
      <span class="h6 signup-or-separator--text">{{ trans('messages.login.or') }}</span>
      <hr>
    </div>

    <div class="text-center">
      <a href="" class="create-using-email btn-block  row-space-2 btn btn-primary btn-block btn-large large icon-btn pad-23 signup_popup_head2" id="create_using_email_button">
          <i class="icon icon-envelope"></i>
          {{ trans('messages.login.signup_with') }} {{ trans('messages.login.email') }}
</a>    </div>

    <div id="tos_outside" class="row-space-top-3">
       <small class="small-font">
  {{ trans('messages.login.signup_agree') }} {{ $site_name }}'s <a href="{{URL::to('/')}}/terms_of_service" data-popup="true">{{ trans('messages.login.terms_service') }}</a>, <a href="{{URL::to('/')}}/privacy_policy" data-popup="true">{{ trans('messages.login.privacy_policy') }}</a>, <a href="{{URL::to('/')}}/guest_refund" data-popup="true">{{ trans('messages.login.guest_policy') }}</a>, {{ trans('messages.header.and') }} <a href="{{URL::to('/')}}/host_guarantee" data-popup="true">{{ trans('messages.login.host_guarantee') }}</a>.
</small>

     </div>
</div>

<div class="panel-body">
  {{ trans('messages.login.already_an') }} {{ $site_name }} {{ trans('messages.login.member') }}
    <a href="{{ url('login') }}" class="modal-link link-to-login-in-signup login-btn login_popup_head" data-modal-href="/login_modal?" data-modal-type="login">
      {{ trans('messages.header.login') }}
</a></div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="login-close">
<div class="signup_popup2">
<div class="page-container-responsive page-container-auth row-space-top-4 row-space-8 margintop">
  <div class="row">
    <div class="col-md-6 col-lg-5 col-center">
      <div class="panel top-home bor-none">
     <!--  <div class="login-close">
        <img src="images/close.png">
      </div>-->
<div class="alert alert-with-icon alert-error alert-header panel-header hidden-element notice" id="notice">
  <i class="icon alert-icon icon-alert-alt"></i>
  
</div>
<div class="panel-padding panel-body pad-25">

  <div class="social-buttons hide">

      
<a href="{{ $fb_url }}" class="fb-button fb-blue btn icon-btn btn-block row-space-1 btn-large btn-facebook" data-populate_uri="" data-redirect_uri="{{URL::to('/')}}/authenticate">
  <span class="icon-container">
    <i class="icon icon-facebook"></i>
  </span>
  <span class="text-container">
    {{ trans('messages.login.signup_with') }} Facebook
  </span>
</a>

      
<a href="{{URL::to('googleLogin')}}" class="btn icon-btn btn-block row-space-1 btn-large btn-google">
  <span class="icon-container">
    <i class="icon icon-google-plus"></i>
  </span>
  <span class="text-container">
    {{ trans('messages.login.signup_with') }} Google
  </span>
</a>
  </div>

  <div class="text-center social-links">
    {{ trans('messages.login.signup_with') }} <a href="{{ $fb_url }}" class="facebook-link-in-signup">Facebook</a> {{ trans('messages.login.or') }} <a href="{{URL::to('googleLogin')}}">Google</a>
  </div>

    <div class="signup-or-separator">
      <span class="h6 signup-or-separator--text">{{ trans('messages.login.or') }}</span>
      <hr>
    </div>

    <div class="text-center">
      <a href="{{URL::to('/')}}/signup_login?sm=2" class="create-using-email btn-block  row-space-2  icon-btn hide" id="create_using_email_button">
          <i class="icon icon-envelope"></i>
          {{ trans('messages.login.signup_with') }} {{ trans('messages.login.email') }}
</a>    </div>

    {!! Form::open(['action' => 'UserController@create', 'class' => 'signup-form', 'data-action' => 'Signup', 'id' => 'user_new', 'accept-charset' => 'UTF-8' , 'novalidate' => 'true']) !!}

    <div class="signup-form-fields">

      {!! Form::hidden('from', 'email_signup', ['id' => 'from']) !!}
      
<div class="control-group row-space-1" id="inputFirst">

  @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
  
  {!! Form::text('first_name', '', ['class' =>  $errors->has('first_name') ? 'decorative-input invalid ' : 'decorative-input name-icon', 'placeholder' => trans('messages.login.first_name')]) !!}

</div>

<div class="control-group row-space-1" id="inputLast">
  
  @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif

  {!! Form::text('last_name', '', ['class' => $errors->has('last_name') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore name-icon', 'placeholder' => trans('messages.login.last_name')]) !!}

</div>

<div class="control-group row-space-1" id="inputEmail">

  @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif

  {!! Form::email('email', '', ['class' => $errors->has('email') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore name-mail name-icon', 'placeholder' => trans('messages.login.email_address')]) !!}

</div>

<div class="control-group row-space-1" id="inputPassword">

  @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif

  {!! Form::password('password', ['class' => $errors->has('password') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore name-pwd name-icon', 'placeholder' => trans('messages.login.password'), 'id' => 'user_password', 'data-hook' => 'user_password']) !!}
  
  <div data-hook="password-strength" class="password-strength hide"></div>
</div>


<div class="control-group row-space-top-3 row-space-1">
  <strong>{{ trans('messages.login.birthday') }}</strong>
  <strong data-behavior="tooltip" aria-label="To sign up, you must be 18 or older. Other people won’t see your birthday.">
    <!--<i class="icon icon-question"></i>-->
  </strong>
</div>

<div class="control-group row-space-1" id="inputBirthday"></div>

@if ($errors->has('birthday_month') || $errors->has('birthday_day') || $errors->has('birthday_year')) <p class="help-block">{{ $errors->has('birthday_month') ? $errors->first('birthday_month') : ($errors->has('birthday_day')) ? $errors->first('birthday_day') : ($errors->has('birthday_year')) ? $errors->first('birthday_year') : '' }}</p> @endif

<div class="control-group row-space-2">
  
  <div class="select month">
{!! Form::selectMonthWithDefault('birthday_month', null, trans('messages.header.month'), [ 'class' => $errors->has('birthday_month') ? 'invalid' : '', 'id' => 'user_birthday_month']) !!}
  </div>
  
  <div class="select day month">
{!! Form::selectRangeWithDefault('birthday_day', 1, 31, null, trans('messages.header.day'), [ 'class' => $errors->has('birthday_day') ? 'invalid' : '', 'id' => 'user_birthday_day']) !!}
  </div>
  
  <div class="select month">
{!! Form::selectRangeWithDefault('birthday_year', date('Y'), date('Y')-120, null, trans('messages.header.year'), [ 'class' => $errors->has('birthday_year') ? 'invalid' : '', 'id' => 'user_birthday_year']) !!}
  </div>
  
</div>

<!-- <label class="pull-left checkbox" style="float:left;">

{!! Form::hidden('user_profile_info[receive_promotional_email]', '0') !!}

{!! Form::checkbox('user_profile_info[receive_promotional_email]', '1', 'true', ['id' => 'user_profile_info_receive_promotional_email']) !!}

 </label> -->
<!-- <label for="user_profile_info_receive_promotional_email" class="checkbox">
 I’d like to receive coupons and inspiration
</label> -->

      <div id="tos_outside" class="row-space-top-3 chk-box">
       <small>
  {{ trans('messages.login.signup_agree') }} {{ $site_name }}'s <a href="{{URL::to('/')}}/terms_of_service" data-popup="true">{{ trans('messages.login.terms_service') }}</a>, <a href="{{URL::to('/')}}/privacy_policy" data-popup="true">{{ trans('messages.login.privacy_policy') }}</a>, <a href="{{URL::to('/')}}/guest_refund" data-popup="true">{{ trans('messages.login.guest_policy') }}</a>, {{ trans('messages.header.and') }} <a href="{{URL::to('/')}}/host_guarantee" data-popup="true">{{ trans('messages.login.host_guarantee') }}</a>.
</small>

     </div>
    {!! Form::submit('Sign up', ['class' => 'btn btn-primary btn-block btn-large pad-top']) !!}

    </div>
    {!! Form::close() !!}
</div>

<div class="panel-body font-small">
  {{ trans('messages.login.already_an') }} {{ $site_name }} {{ trans('messages.login.member') }}
    <a href="{{ url('login') }}" class="modal-link link-to-login-in-signup login-btn login_popup_head" data-modal-href="/login_modal?" data-modal-type="login">
      {{ trans('messages.header.login') }}
</a></div>

      </div>
    </div>
  </div>
</div>
</div>
</div>
<div class="login-close">
<div class="forgot-passward">
 <div class="page-container-responsive page-container-auth row-space-top-4 row-space-8">
  <div class="row">
    <div class="col-md-6 col-lg-5 col-center">
      <div class="panel top-home">
      
        {!! Form::open(['url' => url('forgot_password')]) !!}
  <div id="forgot_password_container">
    <h3 class="panel-header panel-header-gray">
      {{ trans('messages.login.reset_pwd') }}
    </h3>
    <div class="panel-padding panel-body">
      <p>{{ trans('messages.login.reset_pwd_desc') }}</p>
      @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
      <div id="inputEmail" class="textInput text-input-container">
        {!! Form::email('email', '', ['placeholder' => trans('messages.login.email'), 'id' => 'forgot_email', 'class' => $errors->has('email') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore']) !!}
      </div>
      <hr>
      <button class="btn btn-primary" type="submit">
        {{ trans('messages.login.send_reset_link') }}
      </button>
    </div>
  </div>
{!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
</div>
</div>
