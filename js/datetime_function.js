Date.prototype.monthNames = [
    "January", "February", "March",
    "April", "May", "June",
    "July", "August", "September",
    "October", "November", "December"
];

Date.prototype.dayName = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

Date.prototype.getMonthName = function() {
    return this.monthNames[this.getMonth()];
};
Date.prototype.getDayName = function() {
    return this.dayName[this.getDay()];
};
Date.prototype.getShortMonthName = function () {
    return this.getMonthName().substr(0, 3);
};
Date.prototype.getShortDayName = function () {
    return this.getDayName().substr(0, 3);
};


function timepopulate(selector, option_class, timestart, duration) {
    var select = $(selector);
    var hours, minutes, ampm;
    console.log(timestart)
    $(selector).html('')
    duration_count = 0;
    var duration_value = ""
    for(var i = parseInt(timestart); i <= 1435; i += 15){

        duration_count += 15
        if( duration == true ){
            if( duration_count > 45) {
                dhour = Math.floor( duration_count / 60 )
                dmin = duration_count % 60
                duration_value = dmin > 0 ? dhour+"."+dmin : dhour
                duration_value += dhour > 1 ? " hrs" : "hr"
            }  else {
                duration_value = duration_count+' min'
            }   
            
            duration_value = "("+duration_value+")"  
        }

        hours = Math.floor(i / 60);
        realhour = hours
        minutes = i % 60;
        if (minutes < 10){
            minutes = '0' + minutes; // adding leading zero
        }
        ampm = hours % 24 < 12 ? 'AM' : 'PM';
        hours = hours % 12;duration_count
         if (hours === 0){
            hours = 12;
        }
        mil_time = realhour+':'+minutes+':00'

        select.append($('<label class="'+option_class+' time-option"  data-time="'+i+'" data-value="'+mil_time+'"></label>')
            .attr('data-hour', i)
            .text(hours + ':' + minutes + ' ' + ampm+' '+duration_value)); 
    }
}